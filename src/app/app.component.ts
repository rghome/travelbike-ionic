import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from 'ionic-native';
import { WelcomePage } from '../pages/welcome/welcome';
import { TranslateService } from 'ng2-translate/ng2-translate';
import { AuthService } from '../providers/auth-service';


@Component({
  template: `<ion-nav [root]="rootPage"></ion-nav>`
})
export class MyApp {
  rootPage = WelcomePage;

  constructor(private platform: Platform, private translate: TranslateService) {
    this.initializeApp(platform);
    this.translateConfig();
  }

  initializeApp(platform) {
    platform.ready().then(() => {
      StatusBar.styleDefault();
    });
  }

  authController() {

  }

  translateConfig() {
    let userLang = navigator.language.split('-')[0]; // use navigator lang if available
    userLang = /(de|en|ua)/gi.test(userLang) ? userLang : 'en';

    // this language will be used as a fallback when a translation isn't found in the current language
    this.translate.setDefaultLang('en');

    // the lang to use, if the lang isn't available, it will use the current loader to get them
    this.translate.use(userLang);
  }
}

export class AuthApp {
  rootPage = WelcomePage;

  constructor(platform: Platform, private auth: AuthService) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();

      // Schedule a token refresh on app start up
      auth.startupTokenRefresh();
    });
  }
}
