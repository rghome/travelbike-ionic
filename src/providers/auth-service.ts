import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ENV } from '../config/environment.dev';
import { Storage } from '@ionic/storage';
import { JwtHelper, tokenNotExpired } from 'angular2-jwt';
import { UserEntity } from '../entity/user';
import 'rxjs/add/operator/map';

@Injectable()
export class AuthService {
  token: string;

  /**
   * @type {UserEntity}
   */
  currentUser: UserEntity;

  /**
   * @type {JwtHelper}
   */
  jwtHelper: JwtHelper = new JwtHelper();

  /**
   * @type {Storage}
   */
  storage: Storage = new Storage();

  /**
   * @type {Observable}
   */
  observer: any;

  refreshSubscription: any;

  canRefresh: boolean = true;

  /**
   *
   * @param http
   */
  constructor(private http:Http) {
    this.storage.get('token').then((val) => {
      if (val) {
        this.token = val;

        let payload = this.decodeToken();
        this.setUserInfo(payload);
      }
    });
  }

  /**
   *
   * @param credentials
   * @returns {any}
   */
  public login(credentials) {
    if (credentials._username === null || credentials._password === null) {
      return Observable.throw("Please insert credentials");
    } else {
      return Observable.create(observer => {
        this.observer = observer;
        let data = new URLSearchParams();
        data.append('_username', credentials._username);
        data.append('_password', credentials._password);

        let headers = new Headers({
          'Content-Type': 'application/x-www-form-urlencoded'
        });
        let options = new RequestOptions({
          headers: headers
        });

        this.http.post(ENV.API_URL + '/login_check', data, options)
          .map(res => res.json())
          .subscribe(data => this.authenticate(data),
            err => {
              observer.next(false);
              observer.complete();
            },
          )
        ;
      });
    }
  }

  /**
   * @param data
   */
  private authenticate(data) {
    this.token = data.token;

    let payload = this.decodeToken();

    this.setUserInfo(payload);
    this.storage.set('token', this.token);
    this.storage.set('refresh_token', data.refresh_token);
    this.storage.set('payload', payload);

    if (this.observer) {
      this.observer.next(true);
      this.observer.complete();

      this.observer = null;
    }
  }

  /**
   * @returns {any}
   */
  private decodeToken() {
    return this.jwtHelper.decodeToken(this.token);
  }

  /**
   *
   * @returns {Promise<boolean>|Promise<TResult2|boolean>|Promise<any>}
   */
  public isAuth() : Promise<any> {
    return this.storage.get('token').then((val) => {
      return tokenNotExpired('id_token', val);
    });
  }

  public scheduleRefresh() {
    // If the user is authenticated, use the token stream
    // provided by angular2-jwt and flatMap the token

    if (this.refreshSubscription) return false;
    let source = Observable.of(this.token).flatMap(
      token => {
        // The delay to generate in this case is the difference
        // between the expiry time and the issued at time
        let jwtIat = this.jwtHelper.decodeToken(token).iat;
        let jwtExp = this.jwtHelper.decodeToken(token).exp;
        let iat = new Date(0);
        let exp = new Date(0);

        let delay = (exp.setUTCSeconds(jwtExp) - iat.setUTCSeconds(jwtIat));

        return Observable.interval(delay);
      });

    this.refreshSubscription = source.subscribe(() => {
      this.getNewJwt();
    });
  }

  public unscheduleRefresh() {
    // Unsubscribe fromt the refresh
    if (this.refreshSubscription) {
      this.refreshSubscription.unsubscribe();
    }
  }

  public startupTokenRefresh() {
    // If the user is authenticated, use the token stream
    // provided by angular2-jwt and flatMap the token
    this.isAuth().then(auth => {
      if (auth) {
        let source = Observable.of(this.token).flatMap(
          token => {
            // Get the expiry time to generate
            // a delay in milliseconds
            let now: number = new Date().valueOf();
            let jwtExp: number = this.jwtHelper.decodeToken(token).exp;
            let exp: Date = new Date(0);
            exp.setUTCSeconds(jwtExp);
            let delay: number = exp.valueOf() - now;

            // Use the delay in a timer to
            // run the refresh at the proper time
            return Observable.timer(delay);
          });

        // Once the delay time from above is
        // reached, get a new JWT and schedule
        // additional refreshes
        source.subscribe(() => {
          this.getNewJwt();
          this.scheduleRefresh();
        });
      }
    })
  }

  private getNewJwt() {
    if (!this.canRefresh) return false;
    this.canRefresh = false;

    this.storage.get('refresh_token').then(token => {
      let data = new URLSearchParams();
      data.append('refresh_token', token);

      let headers = new Headers({
        'Content-Type': 'application/x-www-form-urlencoded'
      });
      let options = new RequestOptions({
        headers: headers
      });

      this.http.post(ENV.API_URL + '/token/refresh', data, options)
        .map(res => res.json())
        .subscribe(data => {
          this.authenticate(data);
          this.canRefresh = true;
        }, err => {
          this.canRefresh = false;
          this.logout();
        })
      ;
    });
  }

  /**
   *
   * @param credentials
   * @returns {any}
   */
  public register(credentials) {
    if (credentials.email === null || credentials.password === null) {
      return Observable.throw("Please insert credentials");
    } else {
      // At this point store the credentials to your backend!
      return Observable.create(observer => {
        observer.next(true);
        observer.complete();
      });
    }
  }

  /**
   *
   * @returns {UserEntity}
   */
  public getUserInfo() : UserEntity {
    return this.currentUser;
  }

  /**
   *
   * @param data
   */
  public setUserInfo(data) {
    this.currentUser = new UserEntity(data.username, data.email);
    this.scheduleRefresh();
  }

  /**
   *
   * @returns {any}
   */
  public logout() {
    return Observable.create(observer => {
      this.token = null;
      this.currentUser = null;

      this.storage.clear();

      this.unscheduleRefresh();

      observer.next(true);
      observer.complete();
    });
  }
}
