import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service';
import { LoginPage } from '../login/login';
import { HomePage } from '../home/home';

/*
  Generated class for the Welcome page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-welcome',
  templateUrl: 'welcome.html'
})
export class WelcomePage {

  constructor(public navCtrl: NavController, private auth: AuthService) {
    auth.isAuth().then(auth => {
      if (auth) {
        this.navCtrl.setRoot(HomePage);
      } else {
        this.navCtrl.setRoot(LoginPage);
      }
    });
  }
}
